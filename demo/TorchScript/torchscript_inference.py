import argparse
import math
import os
import time

import cv2
from PIL import Image
import numpy as np
import torch
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader

from loguru import logger
from yolox.data.datasets import COCO_CLASSES
from yolox.utils.boxes import postprocess
from yolox.utils.metric import AverageMeter
from yolox.utils.visualize import vis


def make_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_dir", type=str, default="datasets/minival/minival")
    parser.add_argument("--model_path", type=str, default="yoloxts_p_d_b4_cuda_fp16.pt")
    parser.add_argument("--output_dir", type=str, default="datasets/minival/vis_res")
    parser.add_argument("--batch_size", type=int, default=4)
    parser.add_argument("--device", type=str, default="cuda")
    parser.add_argument("--fp16", action="store_true", help="run model in fp16")
    return parser

def get_images_list(folder_path):
    img_list = []
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            img_path = os.path.relpath(os.path.join(root, file), folder_path)
            img_list.append(img_path)
    return img_list

def resize(tensor, target_size=(640,640)):
    if tensor.dim() != 4:
        tensor.unsqueeze_(0)
    scale_y = tensor.size(2) / target_size[0]
    scale_x = tensor.size(3) / target_size[1]
    resize_scale = torch.tensor([scale_x,scale_y,scale_x,scale_y])
    if scale_x != 1 or scale_y != 1:
        tensor = F.interpolate(
            tensor, size=target_size, mode="bilinear", align_corners=False
        )
    tensor.squeeze_(0)
    return tensor, resize_scale


class MinivalDataset(Dataset):
    def __init__(self, folder_path, image_paths, preproc=True):
        self.folder_path = folder_path
        self.image_paths = image_paths
        self.preproc=preproc
    
    def __len__(self):
        return len(self.image_paths)
    
    def __getitem__(self, idx):
        start_preproc = time.time()
        image_path = os.path.join(self.folder_path, self.image_paths[idx])
        image = cv2.imread(image_path)
        image = np.transpose(image,[2,0,1])
        image, resize_scale = resize(torch.from_numpy(image).float())
        return image, image_path, resize_scale, start_preproc


def main():
    am_preproc = AverageMeter()
    am_main = AverageMeter()
    am_inference = AverageMeter()
    am_nms = AverageMeter()
    am_vis = AverageMeter()
    
    args = make_parser().parse_args()
    dataset_dir = args.dataset_dir
    output_dir = args.output_dir
    model_path = args.model_path
    image_paths = get_images_list(dataset_dir)
    device = args.device
    batch_size = args.batch_size

    dataset = MinivalDataset(dataset_dir, image_paths)

    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=0, drop_last=True)

    model = torch.jit.load(model_path)
    model = model.to(device)
    logger.info("loading model done")

    for images, image_path, resize_scales, start_preproc in data_loader:
        images = images.to(device)
        resize_scales = resize_scales.to(device)
        if args.fp16:
            images = images.to(torch.half)
            resize_scales = resize_scales.to(torch.half)
        
        am_preproc.update(time.time() - start_preproc[0])
        
        start = time.time()
        with torch.no_grad():
            prediction = model(images)
        am_inference.update(time.time()-start)

        start = time.time()
        prediction = postprocess(prediction, num_classes=80, conf_thre=0.7, nms_thre=0.45)
        am_nms.update(time.time()-start)
        
        start = time.time()
        # images = images.cpu().numpy().astype(np.uint8)
        # images = np.transpose(images,[0,2,3,1])

        for idx, image_path in enumerate(image_path):
            bboxes = prediction[idx][:, 0:4]*resize_scales[idx]
            # bboxes = prediction[idx][:, 0:4]
            cls = prediction[idx][:, 6]
            scores = prediction[idx][:, 4] * prediction[idx][:, 5]
            cls_names=COCO_CLASSES
            image = cv2.imread(image_path)  
            # image = images[idx].copy()

            vis_res = vis(image, bboxes, scores, cls, 0, cls_names)

            output_path = os.path.join(output_dir, os.path.basename(image_path))
            cv2.imwrite(output_path, vis_res)
        am_vis.update(time.time()-start)
        am_main.update(time.time()-start_preproc[0])
        
    logger.info("all_images saved to {}".format(output_dir))

    print(f"Preprocess: {round(am_preproc.median*1000/batch_size)} ms")
    print(f"Inference: {round(am_inference.median*1000/batch_size)} ms")
    print(f"NMS: {round(am_nms.median*1000/batch_size)} ms")
    print(f"Visualization: {round(am_vis.median*1000/batch_size)} ms")
    print(f"Overall time: {round(am_main.median*1000/batch_size)} ms")

    number_of_processed_imgs = math.floor(len(image_paths)/batch_size)*batch_size
    print(f"Number of processed images: {number_of_processed_imgs}\\")

    print(f"Preprocess: {round(am_preproc.avg*1000/batch_size)} ms")
    print(f"Inference: {round(am_inference.avg*1000/batch_size)} ms")
    print(f"NMS: {round(am_nms.avg*1000/batch_size)} ms")
    print(f"Visualization: {round(am_vis.avg*1000/batch_size)} ms")
    print(f"Overall time: {round(am_main.avg*1000/batch_size)} ms")



if __name__ == "__main__":
    main()
